package com.sachsezhang;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest/") /* This initializes JAX-RS (Resteasy) for the entire application */
public class JAXActivator extends Application {
}
