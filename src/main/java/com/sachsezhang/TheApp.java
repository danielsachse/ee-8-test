package com.sachsezhang;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.time.Instant;

@Path("/")
public class TheApp {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getTime() {
        return "It is now " + Instant.now();
    }
}
