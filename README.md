# EE 8 test #

This should build a deployable EE 8 application that can be run on e.g. EAP 7.3.0

Should come up as http://localhost:8080/ee-8-test/ and https://localhost:8443/ee-8-test/ and
show a welcome page using JSF.

The welcome page contains a link to a JAX-RS resource anchored at <contextPath>/rest/.